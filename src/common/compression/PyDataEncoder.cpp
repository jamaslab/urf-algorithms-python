#include "urf/algorithms/compression/PyDataEncoder.hpp"

#include <urf/algorithms/compression/DataCompressionFactory.hpp>

namespace urf {
namespace algorithms {
namespace compression {

PyDataEncoder::PyDataEncoder(const std::string& method)
    : encoder_() {
    encoder_ = std::move(DataCompressionFactory::getEncoder(method, false));
}

pybind11::bytes PyDataEncoder::encode(pybind11::bytes bytes) {
    std::string_view view(bytes);
    auto encoded = encoder_->encode(std::vector<uint8_t>(view.begin(), view.end()));
    return pybind11::bytes(std::string(encoded.begin(), encoded.end()));
}

std::string PyDataEncoder::getName() {
    return encoder_->getName();
}

} // namespace compression
} // namespace algorithms
} // namespace urf
