#include "urf/algorithms/compression/PyVideoEncoder.hpp"

#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

namespace urf {
namespace algorithms {
namespace compression {

PyVideoEncoder::PyVideoEncoder(const std::string& method,
                               const pybind11::tuple& resolution,
                               int fps,
                               int quality,
                               bool lowLatency)
    : encoder_() {
    encoder_ =
        std::move(VideoCompressionFactory::getEncoder(method,
                                                      VideoResolution(resolution[0].cast<uint32_t>(), resolution[1].cast<uint32_t>()),
                                                      fps,
                                                      static_cast<CompressionQuality>(quality),
                                                      lowLatency));
}

bool PyVideoEncoder::addFrame(const VideoFrame& frame) {
    return encoder_->addFrame(frame);
}

pybind11::bytes PyVideoEncoder::getBytes(bool clearBuffer) {
    auto vec = encoder_->getBytes(clearBuffer);
    return pybind11::bytes(std::string(vec.begin(), vec.end()));
}

bool PyVideoEncoder::flush() {
    return encoder_->flush();
}

int PyVideoEncoder::getCompressionQuality() {
    return static_cast<int>(encoder_->getCompressionQuality());
}

pybind11::tuple PyVideoEncoder::getResolution() {
    auto res = encoder_->getResolution();
    return pybind11::make_tuple(res.width, res.height);
}

std::string PyVideoEncoder::getName() {
    return encoder_->getName();
}

} // namespace compression
} // namespace algorithms
} // namespace urf
