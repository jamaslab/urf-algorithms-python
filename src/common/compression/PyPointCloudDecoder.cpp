#include "urf/algorithms/compression/PyPointCloudDecoder.hpp"

#include <urf/algorithms/compression/DataCompressionFactory.hpp>

#include <pybind11/numpy.h>

namespace urf {
namespace algorithms {
namespace compression {

PyPointCloudDecoder::PyPointCloudDecoder(const std::string& method,
                                         const std::string& pointType,
                                         bool octreeCompression)
    : decoder_()
    , pointType_(pointType) {
    decoder_ = std::make_unique<PointCloudDecoder>(
        octreeCompression, std::move(DataCompressionFactory::getDecoder(method, false)));
}

bool PyPointCloudDecoder::addBytes(const pybind11::bytes& bytes) {
    std::string_view view(bytes);
    return decoder_->addBytes(std::vector<uint8_t>(view.begin(), view.end()));
}

std::optional<pybind11::dict> PyPointCloudDecoder::decode(bool clearBuffer) {
    if (pointType_ == "xyz") {
        auto decoded = decoder_->decode<pcl::PointXYZ>(clearBuffer);
        if (!decoded) {
            return std::nullopt;
        }

        pybind11::dict dictionary;
        pybind11::array_t<float> points(
            {decoded->size(), 3},
            {3*sizeof(float), sizeof(float)});

        // auto shape = boost::python::make_tuple(decoded->size(), 3);
        // auto dt = boost::python::numpy::dtype::get_builtin<float>();
        // auto ndPcl = boost::python::numpy::empty(shape, dt);

        // std::memcpy(ndPcl.get_data(), &(decoded->points[0]), decoded->size() * 3);
        dictionary["points"] = points;
        return dictionary;
    }
    // } else if (pointType_ == "xyzrgb") {
    //     auto decoded = decoder_->decode<pcl::PointXYZRGB>(clearBuffer);
    //     if (!decoded) {
    //         return boost::python::object();
    //     }

    //     boost::python::dict dictionary;
    //     auto shape = boost::python::make_tuple(decoded->size(), 3);
    //     auto pointDt = boost::python::numpy::dtype::get_builtin<float>();
    //     auto colorDt = boost::python::numpy::dtype::get_builtin<uint8_t>();
    //     auto points = boost::python::numpy::empty(shape, pointDt);
    //     auto colors = boost::python::numpy::empty(shape, colorDt);

    //     auto pointsPtr = reinterpret_cast<float*>(points.get_data());
    //     auto colorPtr = reinterpret_cast<uint8_t*>(colors.get_data());

    //     for (int i = 0; i < decoded->size(); i++) {
    //         std::memcpy(pointsPtr + i * 3, &decoded->points[i], sizeof(float) * 3);
    //         std::memcpy(
    //             colorPtr + i * 3, &decoded->points[i] + sizeof(float) * 3, sizeof(uint8_t) * 3);
    //     }

    //     dictionary["points"] = points;
    //     dictionary["colors"] = colors;

    //     return dictionary;
    // } else if (pointType_ == "xyzrgba") {
    //     auto decoded = decoder_->decode<pcl::PointXYZRGBA>(clearBuffer);
    //     if (!decoded) {
    //         return boost::python::object();
    //     }

    //     boost::python::dict dictionary;
    //     auto pointDt = boost::python::numpy::dtype::get_builtin<float>();
    //     auto colorDt = boost::python::numpy::dtype::get_builtin<uint8_t>();
    //     auto points =
    //         boost::python::numpy::empty(boost::python::make_tuple(decoded->size(), 3), pointDt);
    //     auto colors =
    //         boost::python::numpy::empty(boost::python::make_tuple(decoded->size(), 4), colorDt);

    //     auto pointsPtr = reinterpret_cast<float*>(points.get_data());
    //     auto colorPtr = reinterpret_cast<uint8_t*>(colors.get_data());

    //     for (int i = 0; i < decoded->size(); i++) {
    //         std::memcpy(pointsPtr + i * 3, &decoded->points[i], sizeof(float) * 3);
    //         std::memcpy(
    //             colorPtr + i * 4, &decoded->points[i] + sizeof(float) * 3, sizeof(uint8_t) * 4);
    //     }

    //     dictionary["points"] = points;
    //     dictionary["colors"] = colors;
    // }
    return std::nullopt;
}

std::string PyPointCloudDecoder::getName() {
    return decoder_->getName();
}

} // namespace compression
} // namespace algorithms
} // namespace urf
