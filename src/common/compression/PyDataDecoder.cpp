#include "urf/algorithms/compression/PyDataDecoder.hpp"

#include <urf/algorithms/compression/DataCompressionFactory.hpp>

namespace urf {
namespace algorithms {
namespace compression {

PyDataDecoder::PyDataDecoder(const std::string& method)
    : decoder_() {
    decoder_ = std::move(DataCompressionFactory::getDecoder(method, false));
}

bool PyDataDecoder::addBytes(const pybind11::bytes& bytes) {
    std::string_view view(bytes);
    return decoder_->addBytes(std::vector<uint8_t>(view.begin(), view.end()));
}

pybind11::bytes PyDataDecoder::decode(bool clearBuffer) {
    auto decoded = decoder_->decode(clearBuffer);
    return pybind11::bytes(std::string(decoded.begin(), decoded.end()));
}

std::string PyDataDecoder::getName() {
    return decoder_->getName();
}

} // namespace compression
} // namespace algorithms
} // namespace urf
