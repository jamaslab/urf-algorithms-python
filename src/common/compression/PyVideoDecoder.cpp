#include "urf/algorithms/compression/PyVideoDecoder.hpp"
#include <urf/algorithms/compression/VideoCompressionFactory.hpp>

namespace urf {
namespace algorithms {
namespace compression {

PyVideoDecoder::PyVideoDecoder(const std::string& method)
    : decoder_() {
    decoder_ = std::move(VideoCompressionFactory::getDecoder(method));
}

bool PyVideoDecoder::addBytes(const pybind11::bytes& bytes) {
    std::string_view view(bytes);
    return decoder_->addBytes(std::vector<uint8_t>(view.begin(), view.end()));
}

VideoFrame PyVideoDecoder::getFrame() {
    return decoder_->getFrame();
}

bool PyVideoDecoder::clear() {
    return decoder_->clear();
}

} // namespace compression
} // namespace algorithms
} // namespace urf
