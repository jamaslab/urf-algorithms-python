set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(pybind11 REQUIRED)

find_package(Python ${PYBIND11_PYTHON_VERSION} EXACT COMPONENTS Interpreter Development)
find_package(urf_algorithms_cpp REQUIRED)

include_directories()

set(sources
    common/compression/PyDataEncoder.cpp 
    common/compression/PyDataDecoder.cpp
    # common/compression/PyPointCloudDecoder.cpp 
    common/compression/PyVideoEncoder.cpp
    common/compression/PyVideoDecoder.cpp
)

set(libs
    ${urf_algorithms_cpp_LIBRARIES}
    ${pybind11_LIBRARIES}
    ${Python_LIBRARIES}
)

set(include_dirs
    ${urf_algorithms_cpp_INCLUDE_DIRS}
    ${pybind11_INCLUDE_DIRS}
    ${Python_INCLUDE_DIRS})

set(CMAKE_SHARED_MODULE_PREFIX "")

add_library(urf_algorithms_py MODULE ${sources} wrapper.cpp)
target_link_libraries(urf_algorithms_py ${libs})
target_include_directories(urf_algorithms_py PRIVATE ${include_dirs})

pybind11_extension(urf_algorithms_py)
if(NOT MSVC AND NOT ${CMAKE_BUILD_TYPE} MATCHES Debug|RelWithDebInfo)
    # Strip unnecessary sections of the binary on Linux/macOS
    pybind11_strip(urf_algorithms_py)
endif()

set_target_properties(urf_algorithms_py PROPERTIES CXX_VISIBILITY_PRESET "hidden"
                                         CUDA_VISIBILITY_PRESET "hidden")

install(TARGETS urf_algorithms_py DESTINATION urf_algorithms/)

foreach(dependency IN ITEMS ${urf_algorithms_cpp_LIBRARIES})
    if (WIN32)
        string(REPLACE ".lib" ".dll" dependency ${dependency})
    endif(WIN32)
    install(FILES ${dependency} DESTINATION urf_algorithms/ OPTIONAL)
endforeach(dependency)

foreach(dependency IN ITEMS ${urf_common_cpp_LIBRARIES})
    if (WIN32)
        string(REPLACE ".lib" ".dll" dependency ${dependency})
    endif(WIN32)
    install(FILES ${dependency} DESTINATION urf_algorithms/ OPTIONAL)
endforeach(dependency)

install(FILES python/urf_algorithms/__init__.py DESTINATION urf_algorithms/)
install(FILES python/setup.py DESTINATION .)
install(FILES python/MANIFEST.in DESTINATION .)
