#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <memory>

#include "urf/algorithms/compression/IVideoDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class PyVideoDecoder {
 public:
    PyVideoDecoder() = delete;
    PyVideoDecoder(const std::string& method);
    ~PyVideoDecoder() = default;

    bool addBytes(const pybind11::bytes& bytes);
    VideoFrame getFrame();
    bool clear();

private:
    std::unique_ptr<IVideoDecoder> decoder_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
