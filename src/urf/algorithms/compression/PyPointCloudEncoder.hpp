#pragma once

#ifdef _WIN32 || _WIN64
    #include "urf_algorithms_py_export.h"
#else
    #define URF_ALGORITHMS_PY_EXPORT
#endif

#include <boost/python/detail/wrap_python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/python.hpp>

#include <memory>

#include "urf/algorithms/compression/pcl/PointCloudEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class URF_ALGORITHMS_EXPORT PyDataEncoder {
 public:
    PyDataEncoder() = delete;
    PyDataEncoder(const std::string& method, bool octreeCompression);
    ~PyDataEncoder() = default;

    PyObject* encode(PyObject* bytes);
    std::string getName();

private:
    std::shared_ptr<IDataEncoder> encoder_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
