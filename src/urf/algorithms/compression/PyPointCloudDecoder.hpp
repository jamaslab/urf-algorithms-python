#pragma once

#include <pybind11/pybind11.h>

#include <optional>
#include <memory>

#include "urf/algorithms/compression/pcl/PointCloudDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class PyPointCloudDecoder {
 public:
    PyPointCloudDecoder() = delete;
    PyPointCloudDecoder(const std::string& method,
                        const std::string& pointType,
                        bool octreeCompression);
    ~PyPointCloudDecoder() = default;

    bool addBytes(const pybind11::bytes& bytes);
    std::optional<pybind11::dict> decode(bool clearBuffer);
    std::string getName();

 private:
    std::unique_ptr<PointCloudDecoder> decoder_;
    std::string pointType_;
};

} // namespace compression
} // namespace algorithms
} // namespace urf
