#pragma once
#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "urf/algorithms/compression/IDataEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class PyDataEncoder {
 public:
    PyDataEncoder() = delete;
    PyDataEncoder(const std::string& method);
    ~PyDataEncoder() = default;

    pybind11::bytes encode(pybind11::bytes bytes);
    std::string getName();

private:
    std::unique_ptr<IDataEncoder> encoder_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
