#pragma once


#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <memory>

#include "urf/algorithms/compression/IVideoEncoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class PyVideoEncoder {
 public:
    PyVideoEncoder() = delete;
    PyVideoEncoder(const std::string& method, const pybind11::tuple& resolution, int fps, int quality, bool lowLatency);
    ~PyVideoEncoder() = default;

    bool addFrame(const VideoFrame& frame);
    pybind11::bytes getBytes(bool clearBuffer);

    bool flush();
    int getCompressionQuality();
    pybind11::tuple getResolution();
    std::string getName();

 private:
    std::unique_ptr<IVideoEncoder> encoder_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
