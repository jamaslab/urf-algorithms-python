#pragma once
#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "urf/algorithms/compression/IDataDecoder.hpp"

namespace urf {
namespace algorithms {
namespace compression {

class PyDataDecoder {
 public:
    PyDataDecoder() = delete;
    PyDataDecoder(const std::string& method);
    ~PyDataDecoder() = default;

    bool addBytes(const pybind11::bytes& bytes);
    pybind11::bytes decode(bool clearBuffer);
    std::string getName();

private:
    std::unique_ptr<IDataDecoder> decoder_;
};

}  // namespace compression
}  // namespace algorithms
}  // namespace urf
