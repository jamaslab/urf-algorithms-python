from setuptools import setup, find_packages
from setuptools.dist import Distribution

class BinaryDistribution(Distribution):
    """Distribution which always forces a binary package with platform name"""
    def has_ext_modules(foo):
        return True

setup(
    name="urf_algorithms", # Replace with your own username
    version="0.4.0",
    author="Giacomo Lunghi",
    author_email="jamaslab@gmail.com",
    description="Urf algorithms python bindings",
    url="https://gitlab.com/urobf/urf-algorithms-python",
    packages=['urf_algorithms'],
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'opencv-python>=4.5.0',
        'open3d>=0.12.0',
        'numpy>=1.20.3'
    ],
    python_requires='>=3.6',
    distclass=BinaryDistribution
)