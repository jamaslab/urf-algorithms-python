#include <pybind11/pybind11.h>

#include "urf/algorithms/compression/VideoFrame.hpp"

#include "urf/algorithms/compression/PyVideoEncoder.hpp"
#include "urf/algorithms/compression/PyVideoDecoder.hpp"
#include "urf/algorithms/compression/PyDataDecoder.hpp"
#include "urf/algorithms/compression/PyDataEncoder.hpp"

namespace py = pybind11;

using namespace urf::algorithms::compression;

PYBIND11_MODULE(urf_algorithms_py, o) {
    py::class_<VideoFrame>(o, "VideoFrame", py::buffer_protocol())
        .def(py::init<uint32_t, uint32_t, std::string>())
        .def(py::init([](py::buffer b) {
            py::buffer_info info = b.request();
            if (info.format != py::format_descriptor<uint8_t>::format())
                throw std::runtime_error("Incompatible format: expected a unsigned char array!");

            if (info.ndim != 3)
                throw std::runtime_error("Incompatible buffer dimension!");

            if (info.shape[2] != 3) {
                throw std::runtime_error("Invalid shape for numpy");
            }

            VideoFrame frame(info.shape[1], info.shape[0], "bgr24");
            memcpy(
                const_cast<uint8_t*>(frame.data(0)), info.ptr, frame.width() * frame.height() * 3);
            return frame;
        }))
        .def("width", &VideoFrame::width)
        .def("height", &VideoFrame::height)
        .def("stride", &VideoFrame::stride)
        .def("planes", &VideoFrame::planes)
        .def("empty", &VideoFrame::empty)
        .def("pixel_format", &VideoFrame::pixelFormat)
        .def_buffer([](VideoFrame& m) -> py::buffer_info {
            py::buffer_info info;
            info.itemsize = sizeof(uint8_t);
            info.format = py::format_descriptor<uint8_t>::format();
            info.ndim = 3;
            info.shape = {m.height(), m.width(), 3};
            info.strides = {m.width() * 3 * sizeof(uint8_t), 3 * sizeof(uint8_t), sizeof(uint8_t)};
            if (m.planes() == 1) {
                info.ptr = static_cast<void*>(const_cast<uint8_t*>(m.data(0)));
            } else {
                auto converted = m.get<VideoFrame>(m.width(), m.height(), "bgr24");
                info.ptr = static_cast<void*>(const_cast<uint8_t*>(converted.data(0)));
            }

            return info;
        });

    py::class_<PyVideoEncoder>(o, "VideoEncoder")
        .def(py::init<const std::string&, const pybind11::tuple&, int, int, bool>())
        .def("add_frame", &PyVideoEncoder::addFrame)
        .def("get_bytes", &PyVideoEncoder::getBytes, py::arg("clear_buffer") = true)
        .def("flush", &PyVideoEncoder::flush)
        .def("get_compression_quality", &PyVideoEncoder::getCompressionQuality)
        .def("get_name", &PyVideoEncoder::getName)
        .def("get_resolution", &PyVideoEncoder::getResolution);

    py::class_<PyVideoDecoder>(o, "VideoDecoder")
        .def(py::init<const std::string&>())
        .def("add_bytes", &PyVideoDecoder::addBytes)
        .def("get_frame", &PyVideoDecoder::getFrame)
        .def("clear", &PyVideoDecoder::clear);

    py::class_<PyDataEncoder>(o, "DataEncoder")
        .def(py::init<const std::string&>())
        .def("encode", &PyDataEncoder::encode)
        .def("get_name", &PyDataEncoder::getName);

    py::class_<PyDataDecoder>(o, "DataDecoder")
        .def(py::init<const std::string&>())
        .def("add_bytes", &PyDataDecoder::addBytes)
        .def("decode", &PyDataDecoder::decode, py::arg("clear_buffer") = true)
        .def("get_name", &PyDataDecoder::getName);
}